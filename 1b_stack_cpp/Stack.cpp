#include "Stack.hpp"
#include <cstdlib>

#define INIT_SIZE 1
#define GROW_FACTOR 2

Stack::Stack() {
    void* tmp = std::malloc(INIT_SIZE*sizeof(int));
    if(tmp == NULL) std::abort();
    data = (int*)tmp;
    size = INIT_SIZE;
    top = 0;
}

Stack::~Stack() {
    std::free(data);
}

void Stack::push(int value) {
    if(top >= size) {
        grow();
    }
    data[top] = value;
    top++;
}

int Stack::pop() {
    if(top > 0) {
        top--;
        return data[top];
    }
    std::abort();
}

int Stack::peek() {
    if(top > 0) {
        return data[top-1];
    }
    std::abort();
}

bool Stack::isEmpty() {
    if(top == 0) {
        return true;
    } else {
        return false;
    }
}

void Stack::grow() {
    void* tmp = realloc(data, GROW_FACTOR*size*sizeof(int));
        if(tmp == NULL) {
            std::free(data);
            std::abort();
        }
    data = (int*)tmp;
    size = GROW_FACTOR*size;
}
