#include <iostream>

#include "Stack.hpp"

int main() {
    Stack s1;
    Stack s2;
    std::cout << s1.isEmpty() << std::endl;
    s1.push(2);
    s1.push(3);
    s2.push(4);
    s2.push(5);
    std::cout << s1.isEmpty() << std::endl;
    std::cout << "Stack 1" << std::endl;
    std::cout << s1.pop() << std::endl;
    std::cout << s1.pop() << std::endl;
    std::cout << "Stack 2" << std::endl;
    std::cout << s2.pop() << std::endl;
    std::cout << s2.pop() << std::endl;

}
