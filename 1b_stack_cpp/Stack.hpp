#pragma once
class Stack {
    private:
        int* data;
        unsigned int size;
        unsigned int top;
        
        void grow();
        
    public:
        Stack();
        ~Stack();
        void push(int value);
        int pop();
        int peek();
        bool isEmpty();
};
