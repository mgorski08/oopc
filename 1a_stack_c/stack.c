#include <stdlib.h>
#include "stack.h"

void push(int value, Stack* stack) {
    if(stack->top >= stack-> size) {
        int* tmp = realloc(stack->data, 2*stack->size*sizeof(int));
        if(tmp == NULL) {
            free(stack->data);
            abort();
        }
        stack->data = tmp;
    }
    stack->data[stack->top] = value;
    stack->top++;
}

int pop(Stack* stack) {
    if(stack->top > 0) {
        stack->top--;
        return stack->data[stack->top];
    }
    abort();
}

int peek(Stack* stack) {
    if(stack->top > 0) {
        return stack->data[stack->top-1];
    }
    abort();
}

void init(Stack* stack) {
    stack->data = malloc(50*sizeof(int));
    stack->size = 50;
    stack->top = 0;
}

void destroy(Stack* stack) {
    free(stack->data);
}
