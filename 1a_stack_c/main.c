#include <stdlib.h>
#include <stdio.h>

#include "stack.h"

int main() {
    Stack s1;
    init(&s1);
    Stack s2;
    init(&s2);
    push(2, &s1);
    push(3, &s1);
    push(4, &s2);
    push(5, &s2);
    printf("Stack 1\n");
    printf("%d\n", pop(&s1));
    printf("%d\n", pop(&s1));
    printf("Stack 2\n");
    printf("%d\n", pop(&s2));
    printf("%d\n", pop(&s2));
    destroy(&s1);
    destroy(&s2);
}
