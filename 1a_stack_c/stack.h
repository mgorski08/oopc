typedef struct {
    int* data;
    unsigned int size;
    unsigned int top;
} Stack;

void push(int value, Stack* stack);
int pop(Stack* stack);
int peek(Stack* stack);
void init(Stack* stack);
void destroy(Stack* stack);
