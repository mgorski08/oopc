#include <fstream>
#include <iostream>
#include "Matrix.hpp"


Matrix::Matrix(unsigned int width, unsigned int height) {
    mData = new struct MData(width, height);
}

Matrix::Matrix(const Matrix& m) {
    mData = m.mData;
    mData->references++;
}

Matrix::Matrix(std::ifstream& stream) {
    unsigned int height;
    stream >> height;
    unsigned int width;
    stream >> width;
    mData = new struct MData(width, height);
    for(unsigned int i = 0 ; i < height ; i++) {
        for(unsigned int j = 0 ; j < width ; j++) {
            stream >> mData->data[i][j];
        }
    }
}

Matrix& Matrix::operator=(Matrix m) {
    mData->references--;
    if(mData->references < 1) delete mData;
    mData = m.mData;
    mData->references++;
    return *this;
}

std::ostream& operator<<(std::ostream& stream, Matrix m) {
    for(unsigned int i = 0 ; i < m.mData->height ; i++) {
        for(unsigned int j = 0 ; j < m.mData->width ; j++) {
            stream << m(i, j) << " ";
        }
        stream << std::endl;
    }
    stream << std::endl;
    return stream;
}

Matrix::~Matrix() {
    mData->references--;
    if(mData->references < 1) delete mData;
}

Matrix& Matrix::operator+=(Matrix m) {
    if(mData->height != m.mData->height || mData->width != m.mData->width) {
        throw std::string("Matrix size mismatch.");
    }
    for(unsigned int i = 0 ; i < mData->height ; i++) {
        for(unsigned int j = 0 ; j < mData->width ; j++) {
             (*this)(i, j) = (*this)(i, j) + m(i, j);
        }
    }
    return *this;
}

Matrix& Matrix::operator-=(Matrix m) {
    return *this += -m;
}

Matrix& Matrix::operator*=(Matrix m) {
    *this = *this * m;
    return *this;
}

Matrix Matrix::operator+(Matrix m) {
	Matrix tmp(*this);
	tmp += m;
	return tmp;
}

Matrix Matrix::operator*(Matrix m) {
    if(mData->width != m.mData->height) {
        throw std::string("Matrix size mismatch.");
    }
    unsigned int lostdim = mData->width;
    Matrix tmp(mData->height, m.mData->width);
    for(unsigned int i = 0 ; i < tmp.mData->height ; i++) {
		for(unsigned int j = 0 ; j < tmp.mData->width ; j++) {
			int field = 0;
			for(unsigned int dim = 0 ; dim < lostdim ; dim++) {
				field += (*this)(i, dim) * m(dim, j);
			}
			tmp(i, j) = field;
		}
	}
    return tmp;
}

Matrix Matrix::operator-() {
    Matrix tmp(*this);
    for(unsigned int i = 0 ; i < mData->height ; i++) {
        for(unsigned int j = 0 ; j < mData->width ; j++) {
             tmp(i, j) = -(tmp(i,j));
        }
    }
    return tmp;
}

Matrix Matrix::operator-(Matrix m) {
    return *this + -m;
}

Matrix::Proxy Matrix::operator()(unsigned int i, unsigned int j) {
    return Proxy(*this, i, j);
}

Matrix::MData::MData(unsigned int width, unsigned int height) : width(width), height(height), references(1) {
    data = new int*[height];
    for(unsigned int i = 0 ; i < height ; i++) {
        data[i] = new int[width];
    }
}

Matrix::MData::MData(MData& md) : width(md.width), height(md.height), references(md.references) {
	data = new int*[height];
    for(unsigned int i = 0 ; i < height ; i++) {
        data[i] = new int[width];
    }
    for(unsigned int i = 0 ; i < height ; i++) {
        for(unsigned int j = 0 ; j < width ; j++) {
             data[i][j] = md.data[i][j];
        }
    }
}

Matrix::MData::~MData() {
    for(unsigned int i = 0 ; i < height ; i++) {
        delete[] data[i];
    }
    delete[] data;
}

Matrix::Proxy::operator int() const {
	return Mref.mData->data[i][j];
}

Matrix::Proxy& Matrix::Proxy::operator=(int n) {
    if(Mref.mData->references > 1) {
        MData* tmp = new MData(*(Mref.mData));
        tmp->references = 1;
        tmp->data[i][j] = n;
        Mref.mData->references--;
        if(Mref.mData->references < 1) {
            delete Mref.mData;
        }
        Mref.mData = tmp;
    } else {
        Mref.mData->data[i][j] = n;
    }
	return *this;
}

Matrix::Proxy& Matrix::Proxy::operator=(const Proxy& p) {
	return operator=((int)p);
}
