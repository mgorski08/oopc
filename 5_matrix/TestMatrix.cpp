#include <iostream>
#include <fstream>

#include "Matrix.hpp"

int main() {
    try {
        std::ifstream fstream("m.txt");
        std::ifstream fstream2("m2.txt");
        Matrix m(fstream);
        Matrix m2(fstream2);
        Matrix m3 = m2*m;
        Matrix m4 = -m3;
	m4 = m2;
        std::cout << m;
        std::cout << m2;
        std::cout << -m3;
        std::cout << m3 - m4;
    } catch (std::string s) {
        std::cout << s << std::endl;
    }
    return 0;
}
