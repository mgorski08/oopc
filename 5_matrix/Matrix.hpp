#pragma once
#include <fstream>
#include <iostream>


class Matrix {
	class Proxy {
		friend class Matrix;
		
		private:
			Matrix& Mref;
			unsigned int i, j;
			Proxy(Matrix& m, unsigned int i, unsigned int j) : Mref(m), i(i), j(j) {};
		public:
			operator int() const;
			Proxy& operator=(int n);
			Proxy& operator=(const Proxy& p);
	};
	private:
	    struct MData {
	        unsigned const int width;
	        unsigned const int height;
	        unsigned int references;
	        int** data;
	        
	        MData(unsigned int width, unsigned int height);
	        MData(MData& md);
	        ~MData();
	    } * mData;
	public:
	    Matrix(unsigned const int width, unsigned const int height);
	    Matrix(const Matrix& m);
	    Matrix(std::ifstream& stream);
	    ~Matrix();
	    friend std::ostream& operator<<(std::ostream& stream, Matrix m);
	    
	    Matrix& operator=(Matrix m);
	    
	    Matrix& operator+=(Matrix m);
	    Matrix& operator-=(Matrix m);
	    Matrix operator+(Matrix m);
        Matrix operator*(Matrix m);
        Matrix& operator*=(Matrix m);
        Matrix operator-();
        Matrix operator-(Matrix m);
	    Proxy operator()(unsigned int i, unsigned int j);
};

std::ostream& operator<<(std::ostream& stream, Matrix m);
