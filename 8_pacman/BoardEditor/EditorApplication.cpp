#include "EditorApplication.hpp"

EditorApplication::EditorApplication() : Application(),
                                         graphicsOut(768, 576),
                                         needsRedrawing(true),
                                         mainWidget(nullptr, this) {
    pac::Button* button1 = new pac::Button(&mainWidget, this, 50, 20, false);
    pac::Button* button2 = new pac::Button(&mainWidget, this, 50, 20, true);
    pac::BoardWidget* boardW = new pac::BoardWidget(&mainWidget, this, "../game/res/default/");
    button1->moveTo(10,10);
    button2->moveTo(100,10);
    boardW->moveTo(10, 40);
    widgets.push_back(button1);
    widgets.push_back(button2);
    widgets.push_back(boardW);
    mainWidget.addChild(button1);
    mainWidget.addChild(button2);
    mainWidget.addChild(boardW);
    
}

void EditorApplication::procGraphics() {
    if(needsRedrawing) {
        graphicsOut.input(mainWidget.output());
        //needsRedrawing = false;
        needsRedrawing = true;
    }
}


void EditorApplication::actionPerformed(pac::Event* e) {
    pac::Application::actionPerformed(e);
    if(e->type == pac::EventType::BUTTON_PRESSED) {
        ((pac::BoardWidget*)widgets[2])->save();
    }
}
