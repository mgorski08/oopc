#include "EditorApplication.hpp"

int main(int argc, char* argv[]) {
    EditorApplication app;
    return app.exec();
}
