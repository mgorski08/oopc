#pragma once

#include <SDL2/SDL.h>
#include <vector>

#include "../libpacman/include/libpacman.hpp"

class EditorApplication : public pac::Application {
    private:
        pac::WindowedGraphicsOutput graphicsOut;
        bool needsRedrawing;
        pac::HorizontalLayout mainWidget;
        std::vector<pac::Widget*> widgets;

    public:
        EditorApplication();
        void procGraphics();
        void actionPerformed(pac::Event* e);
};
