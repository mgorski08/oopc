#pragma once

#include "../libpacman/include/libpacman.hpp"
#include "Ghost.hpp"

class Blinky : public Ghost {
    public:
        Blinky();
        SDL_Surface* output();
};
