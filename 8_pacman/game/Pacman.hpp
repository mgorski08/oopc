#pragma once

#include "../libpacman/include/libpacman.hpp"
#include "MovingObject.hpp"

class Pacman : public MovingObject {
    public:
        Pacman();
        SDL_Surface* output();
};
