#include "PacmanEngine.hpp"

PacmanEngine::PacmanEngine() : board("res/default/") {
    int boardWidthBx = board.getBoardWidthBx();
    int boardHeightBx = board.getBoardHeightBx();
    int boardWidthPx = board.getBoardWidthPx();
    int boardHeightPx = board.getBoardHeightPx();
    int boxWidthPx = board.getBoxWidthPx();
    int boxHeightPx = board.getBoxHeightPx();
    
    surface = SDL_CreateRGBSurface(0, boardWidthPx, boardHeightPx, 32, 0, 0, 0, 0);
    background = SDL_CreateRGBSurface(0, boardWidthPx, boardHeightPx, 32, 0, 0, 0, 0);
    SDL_Rect dstrect;
    for(int y = 0 ; y < boardHeightBx ; y++) {
        for(int x = 0 ; x < boardWidthBx ; x++) {
            dstrect.x = boxWidthPx*x;
            dstrect.y = boxHeightPx*y;
            SDL_BlitSurface(board.getBoxSurfaceAt(x, y), NULL, background, &dstrect);
        }
    }
    
}

PacmanEngine::~PacmanEngine() {
    SDL_FreeSurface(surface);
}

SDL_Surface* PacmanEngine::output() {
    SDL_BlitSurface(background, NULL, surface, NULL);
    
    advanceMO(&pacman);
    blitMO(&pacman);
    blitMO(&blinky);
    return surface;
}

void PacmanEngine::advanceMO(MovingObject* mo) {
    if(!canMOmove(mo)) {
        pacman.speed = 0;
    }
    
    switch(mo->direction) {
        case MovingObject::Direction::LEFT: {
            mo->xOffset -= mo->speed;
            if(mo->xOffset == -4) {
                --mo->x;
                mo->xOffset = 4;
            }
            break;
        }
        
        case MovingObject::Direction::RIGHT: {
            mo->xOffset += mo->speed;
            if(mo->xOffset == 4) {
                ++mo->x;
                mo->xOffset = -4;
            }
            break;
        }
        
        case MovingObject::Direction::UP: {
            mo->yOffset -= mo->speed;
            if(mo->yOffset == -4) {
                --mo->y;
                mo->yOffset = 4;
            }
            break;
        }
        
        case MovingObject::Direction::DOWN: {
            mo->yOffset += mo->speed;
            if(mo->yOffset == 4) {
                ++mo->y;
                mo->yOffset = -4;
            }
            break;
        }
    }
}

void PacmanEngine::blitMO(MovingObject* mo) {
    SDL_Rect tmpRect;
    tmpRect.x = mo->x*8 + mo->xOffset - 4;
    tmpRect.y = mo->y*8 + mo->yOffset - 4;
    SDL_BlitSurface(mo->output(), NULL, surface, &tmpRect);
}

bool PacmanEngine::canMOmove(MovingObject* mo) {
    switch(mo->direction) {
        case MovingObject::Direction::LEFT:
            return board.getBoxTypeAt(mo->x - 1, mo->y) == 0 ||
                   board.getBoxTypeAt(mo->x - 1, mo->y) == 17 ||
                   mo->xOffset != 0;
                   
        case MovingObject::Direction::RIGHT:
            return board.getBoxTypeAt(mo->x + 1, mo->y) == 0 ||
                   board.getBoxTypeAt(mo->x + 1, mo->y) == 17 ||
                   mo->xOffset != 0;
                   
        case MovingObject::Direction::UP:
            return board.getBoxTypeAt(mo->x, mo->y - 1) == 0 ||
                   board.getBoxTypeAt(mo->x, mo->y - 1) == 17 ||
                   mo->yOffset != 0;
                   
        case MovingObject::Direction::DOWN:
            return board.getBoxTypeAt(mo->x, mo->y + 1) == 0 ||
                   board.getBoxTypeAt(mo->x, mo->y + 1) == 17 ||
                   mo->yOffset != 0;
    }
}
