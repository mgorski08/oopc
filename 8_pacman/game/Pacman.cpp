#include "Pacman.hpp"

Pacman::Pacman() {
    surface = SDL_CreateRGBSurface(0, 16, 16, 32, 0, 0, 0, 0);
    x = 13;
    y = 23;
    xOffset = 0;
    yOffset = 0;
    direction = Direction::UP;
    speed = 1;
}

SDL_Surface* Pacman::output() {
    SDL_FillRect(surface, nullptr, 0xFFFF00);
    return surface;
}
