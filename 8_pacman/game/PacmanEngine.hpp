#ifndef HPP_PACMAN_ENGINE
#define HPP_PACMAN_ENGINE

#include <vector>
#include <SDL2/SDL.h>
#include <fstream>

#include "../libpacman/include/libpacman.hpp"
#include "Blinky.hpp"
#include "Pacman.hpp"


class PacmanEngine {
    protected:
        pac::Board board;
        SDL_Surface* surface;
        SDL_Surface* background;
        Pacman pacman;
        Blinky blinky;
        
        void advanceMO(MovingObject* mo);
        void blitMO(MovingObject* mo);
        bool canMOmove(MovingObject* mo);

    public:
        PacmanEngine();
        ~PacmanEngine();
        SDL_Surface* output();
};

#endif
