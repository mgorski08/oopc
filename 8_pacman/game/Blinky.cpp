#include "Blinky.hpp"

Blinky::Blinky() {
    surface = SDL_CreateRGBSurface(0, 16, 16, 32, 0, 0, 0, 0);
    x = 13;
    y = 16;
    xOffset = 0;
    yOffset = 0;
    direction = Direction::LEFT;
    speed = 1;
}

SDL_Surface* Blinky::output() {
    SDL_FillRect(surface, nullptr, 0xFF0000);
    return surface;
}
