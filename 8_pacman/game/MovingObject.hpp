#pragma once

#include "../libpacman/include/libpacman.hpp"

class MovingObject : public pac::GraphicsOutput {
    protected:
        SDL_Surface* surface;

    public:
        int x;
        int y;
        int xOffset;
        int yOffset;
        int speed;
        enum class Direction {
            UP,
            RIGHT,
            DOWN,
            LEFT
        } direction;
        
};
