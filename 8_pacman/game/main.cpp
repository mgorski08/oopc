#include "GameApplication.hpp"

int main(int argc, char* argv[]) {
    GameApplication app;
    return app.exec();
}
