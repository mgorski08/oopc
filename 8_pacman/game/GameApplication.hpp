#pragma once

#include <SDL2/SDL.h>
#include <vector>

#include "../libpacman/include/libpacman.hpp"
#include "PacmanEngine.hpp"

class GameApplication : public pac::Application {
    private:
        pac::WindowedGraphicsOutput graphicsOut;
        PacmanEngine pacmanEngine;

    public:
        GameApplication();
        void procGraphics();
};
