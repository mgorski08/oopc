#include "include/WindowedGraphicsOutput.hpp"

pac::WindowedGraphicsOutput::WindowedGraphicsOutput(unsigned int width, unsigned int height) {
    if(SDL_Init(SDL_INIT_VIDEO) != 0) {
        throw Exception(SDL_GetError());
    }
    window = SDL_CreateWindow("PacMan", SDL_WINDOWPOS_UNDEFINED,
                              SDL_WINDOWPOS_UNDEFINED, width,
                              height, 0);
    if(window == NULL) {
        throw Exception(SDL_GetError());
    }
    screenSurface = SDL_GetWindowSurface(window);
}

pac::WindowedGraphicsOutput::~WindowedGraphicsOutput() {
    SDL_DestroyWindow(window);
    SDL_Quit();
}

void pac::WindowedGraphicsOutput::input(SDL_Surface* inputSurface) {
    SDL_BlitSurface(inputSurface, NULL, screenSurface, NULL);
    SDL_UpdateWindowSurface(window);
}
