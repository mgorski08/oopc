#include "include/Exception.hpp"

pac::Exception::Exception() {
    message = std::string("");
}

pac::Exception::Exception(std::string message) {
    this->message = message;
}

std::string pac::Exception::getMessage() {
    return message;
}
