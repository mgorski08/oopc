#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "include/libpacman.hpp"

pac::Board::Board(std::string filename) {
    brdPath = filename + "board.brd";
    std::ifstream boardFile(brdPath);
    boardFile >> boardWidthBx;
    boardFile >> boardHeightBx;
    
    boardArray = new int[boardWidthBx*boardHeightBx];
    
    std::string line;
    std::getline(boardFile, line);
    std::getline(boardFile, line);
    while (line != "") {
        names.push_back(line);
        std::cout << (filename + line + ".png") << std::endl;
        boxes.push_back(IMG_Load((filename + line + ".png").c_str()));
        std::getline(boardFile, line);
    }
    boxWidthPx = boxes[0]->w;
    boxHeightPx = boxes[0]->h;
    
    boardWidthPx = boardWidthBx * boxWidthPx;
    boardHeightPx = boardHeightBx * boxHeightPx;
    
    for(int i = 0 ; i < boardWidthBx*boardHeightBx ; i++) {
        boardFile >> std::hex >> boardArray[i];
    }

}

pac::Board::~Board() {
    delete boardArray;
}

int pac::Board::getBoardWidthBx() {
    return boardWidthBx;
}

int pac::Board::getBoardHeightBx() {
    return boardHeightBx;
}

int pac::Board::getBoxWidthPx() {
    return boxWidthPx;
}

int pac::Board::getBoxHeightPx() {
    return boxHeightPx;
}

int pac::Board::getBoardWidthPx() {
    return boardWidthPx;
}

int pac::Board::getBoardHeightPx() {
    return boardHeightPx;
}

SDL_Surface* pac::Board::getBoxSurfaceAt(int x, int y) {
    return boxes[boardArray[x+boardWidthBx*y]];
}

int pac::Board::getBoxTypeAt(int x, int y) {
    return boardArray[x+boardWidthBx*y];
}

void pac::Board::setBoxTypeAt(int x, int y, int type) {
    boardArray[x+boardWidthBx*y] = type;
}

void pac::Board::changeBoxTypeAt(int x, int y, int amount) {
    boardArray[x+boardWidthBx*y] += amount;
    if(boardArray[x+boardWidthBx*y] < 0) {
        boardArray[x+boardWidthBx*y] += boxes.size();
    } else if(boardArray[x+boardWidthBx*y] >= (int)boxes.size()){
        boardArray[x+boardWidthBx*y] -= boxes.size();
    }
}

int pac::Board::getBoxTypesCount() {
    return boxes.size();
}

void pac::Board::save() {
    std::ofstream boardFile(brdPath);
    boardFile << boardWidthBx << std::endl;
    boardFile << boardHeightBx << std::endl;
    for(std::string name : names) {
        boardFile << name << std::endl;
    }
    boardFile << std::endl;
    for(int i = 0 ; i < boardHeightBx ; ++i) {
        for(int j = 0 ; j < boardWidthBx ; ++j) {
            boardFile << std::hex << boardArray[j + boardWidthBx*i] << " ";
        }
        boardFile << std::endl;
    }
    
    
}
