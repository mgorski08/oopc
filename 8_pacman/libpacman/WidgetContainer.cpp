#include "include/WidgetContainer.hpp"
#include "include/MouseEvent.hpp"

void pac::WidgetContainer::dispatchEvent(Event* event) {
    if (event->type == EventType::MOUSE_MOVED) {
        MouseEvent* me = (MouseEvent*) event;
        Widget* child = findChildByPoint(me->x, me->y);
        
        if(child != childInFocus) {
            MouseEvent mel = *me;
            MouseEvent mee = *me;
            mel.type = EventType::MOUSE_EXITED;
            mee.type = EventType::MOUSE_ENTERED;
            sendMouseEventToChild(child, &mee);
            sendMouseEventToChild(childInFocus, &mel);
        }
        sendMouseEventToChild(child, me);
        childInFocus = child;
    } else if(event->type == EventType::MOUSE_PRESSED or event->type == EventType::MOUSE_RELEASED) {
        MouseEvent* me = (MouseEvent*) event;
        Widget* child = findChildByPoint(me->x, me->y);
        sendMouseEventToChild(child, me);
    }
}

void pac::WidgetContainer::registerListener(EventType et, ActionListener* l) {
    //TODO throw not supported
}

void pac::WidgetContainer::addChild(Widget* widget) {
    children.push_back(widget);
}

void pac::WidgetContainer::actionPerformed(Event* e) {
    dispatchEvent(e);
}

bool pac::WidgetContainer::widgetContainsPoint(Widget* w, int x, int y) {
    SDL_Rect rect = w->getRect();
    return (x >= rect.x && x <= rect.x+rect.w && y >= rect.y && y <= rect.y+rect.h);
}

pac::Widget* pac::WidgetContainer::findChildByPoint(int x, int y) {
    for(Widget* child : children) {
        if(widgetContainsPoint(child, x, y)) return child;
    }
    return nullptr;
}

void pac::WidgetContainer::sendMouseEventToChild(Widget* w, MouseEvent* me) {
    if(w == nullptr) return;
    me->x -= w->getRect().x;
    me->y -= w->getRect().y;
    w->actionPerformed(me);
}
