#include "include/Button.hpp"
#include "include/MouseEvent.hpp"
#include "include/WidgetContainer.hpp"


pac::Button::Button(WidgetContainer* parent, Application* app, int w, int h, bool t) : Widget(parent, app) {
    surface = nullptr;
    setSize(w, h);
    isDepressed = false;
    isHighlighted = false;
    isToggleType = t;
    
    testRect.w=10;
    testRect.h=10;
}

void pac::Button::setSize(int w, int h) {
    rect.w = w;
    rect.h = h;
}

void pac::Button::resize(int w, int h) {
    setSize(w, h);
    SDL_FreeSurface(surface);
    surface = nullptr;
}

void pac::Button::drawNormalBorder() {
    SDL_Rect tmpRect;
    tmpRect.x = 0;
    tmpRect.y = 0;
    tmpRect.w = rect.w-1;
    tmpRect.h = 1;
    SDL_FillRect(surface, &tmpRect, 0xFCFCFC);
    tmpRect.w = 1;
    tmpRect.h = rect.h-1;
    SDL_FillRect(surface, &tmpRect, 0xFCFCFC);
    
    tmpRect.x = rect.w-1;
    tmpRect.y = 0;
    tmpRect.w = 1;
    tmpRect.h = rect.h;
    SDL_FillRect(surface, &tmpRect, 0);
    
    tmpRect.x = 0;
    tmpRect.y = rect.h-1;
    tmpRect.w = rect.w;
    tmpRect.h = 1;
    SDL_FillRect(surface, &tmpRect, 0);
    
    tmpRect.x = rect.w-2;
    tmpRect.y = 1;
    tmpRect.w = 1;
    tmpRect.h = rect.h-2;
    SDL_FillRect(surface, &tmpRect, 0x828282);
    
    tmpRect.x = 1;
    tmpRect.y = rect.h-2;
    tmpRect.w = rect.w-2;
    tmpRect.h = 1;
    SDL_FillRect(surface, &tmpRect, 0x828282);
}

void pac::Button::drawDepressedBorder() {
    SDL_Rect tmpRect;
    tmpRect.x = 0;
    tmpRect.y = 0;
    tmpRect.w = rect.w-1;
    tmpRect.h = 1;
    SDL_FillRect(surface, &tmpRect, 0);
    tmpRect.w = 1;
    tmpRect.h = rect.h-1;
    SDL_FillRect(surface, &tmpRect, 0);
    
    tmpRect.x = rect.w-1;
    tmpRect.y = 0;
    tmpRect.w = 1;
    tmpRect.h = rect.h;
    SDL_FillRect(surface, &tmpRect, 0xFFFFFF);
    
    tmpRect.x = 0;
    tmpRect.y = rect.h-1;
    tmpRect.w = rect.w;
    tmpRect.h = 1;
    SDL_FillRect(surface, &tmpRect, 0xFFFFFF);
    
    tmpRect.x = 1;
    tmpRect.y = 1;
    tmpRect.w = 1;
    tmpRect.h = rect.h-2;
    SDL_FillRect(surface, &tmpRect, 0x828282);
    
    tmpRect.x = 1;
    tmpRect.y = 1;
    tmpRect.w = rect.w-2;
    tmpRect.h = 1;
    SDL_FillRect(surface, &tmpRect, 0x828282);
}

void pac::Button::createSurface() {
    if (surface != nullptr) {
        //TODO Log warn
        return;
    }
    surface = SDL_CreateRGBSurface(0, rect.w, rect.h, 32, 0, 0, 0, 0);
}

SDL_Surface* pac::Button::output() {
    if(surface == nullptr) createSurface();
    if(isHighlighted) {
        SDL_FillRect(surface, NULL, 0xD9D9D9);
    } else {
        SDL_FillRect(surface, NULL, 0xC3C3C3);
    }
    if(isDepressed) {
        drawDepressedBorder();
    } else {
        drawNormalBorder();
    }
    //SDL_FillRect(surface, &testRect, 0xFF0000);
    return surface;
}

void pac::Button::actionPerformed(Event* event) {
    MouseEvent* e = (MouseEvent*) event;
    if(e->type == EventType::MOUSE_ENTERED) {
        isHighlighted = true;
    } else if(e->type == EventType::MOUSE_EXITED) {
        isHighlighted = false;
    } else if(e->type == EventType::MOUSE_PRESSED) {
        Event be(EventType::BUTTON_PRESSED);
        app->actionPerformed(&be);
        if(isToggleType) {
            isDepressed = !isDepressed;
        } else {
            isDepressed = true;
        }
    } else if(e->type == EventType::MOUSE_RELEASED) {
        if(!isToggleType) isDepressed = false;
    } else if(e->type == EventType::MOUSE_MOVED) {
        testRect.x = e->x;
        testRect.y = e->y;
    }

}
