#include "include/Application.hpp"
#include "include/MouseEvent.hpp"
#include "include/QuitEvent.hpp"

pac::Application::Application() {
    registerListener(EventType::QUIT, this);
}

pac::Application::~Application() {

}

void pac::Application::dispatchEvent(Event* e) {
    if(listeners.count(e->type) == 1) {
        for(ActionListener* l : listeners.at(e->type)) {
            l->actionPerformed(e);
        }
    }
}

void pac::Application::registerListener(EventType et, ActionListener* l) {
    listeners[et].push_back(l);
}

void pac::Application::actionPerformed(Event* e) {
    switch(e->type) {
        case EventType::QUIT: exit(0); break;
        default: break;
    }
}

int pac::Application::exec() {
    SDL_Event e;
    while(true) {
        while(SDL_PollEvent(&e) != 0) {
            switch (e.type) {
                case SDL_MOUSEMOTION:{
                    MouseEvent me(EventType::MOUSE_MOVED, e.motion.x, e.motion.y);
                    dispatchEvent(&me);
                    break;
                }
                
                case SDL_MOUSEBUTTONDOWN:{
                    MouseEvent me(EventType::MOUSE_PRESSED, e.motion.x, e.motion.y, MouseEvent::mouseButtonFromSDL(e.button.button));
                    dispatchEvent(&me);
                    break;
                }
                
                case SDL_MOUSEBUTTONUP:{
                    MouseEvent me(EventType::MOUSE_RELEASED, e.motion.x, e.motion.y, MouseEvent::mouseButtonFromSDL(e.button.button));
                    dispatchEvent(&me);
                    break;
                }
                
                case SDL_QUIT: {
                    QuitEvent qe(EventType::QUIT);
                    dispatchEvent(&qe);
                    break;
                }
            }
        }
        procGraphics();
        SDL_Delay(20);
    }
}
