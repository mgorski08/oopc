#include "include/Widget.hpp"

pac::Widget::~Widget() {
    //TODO: free surfaces
}

SDL_Rect pac::Widget::getRect() {
    return rect;
}

void pac::Widget::setMargin(pac::Widget::Margin margin) {
    this->margin = margin;
}

pac::Widget::Margin pac::Widget::getMargin() {
    return margin;
}

void pac::Widget::moveTo(int x, int y) {
    rect.x = x;
    rect.y = y;
}
