#include <SDL2/SDL.h>

#include "WidgetContainer.hpp"

namespace pac {

    class HorizontalLayout : public WidgetContainer  {
        private:

        public:
            HorizontalLayout(WidgetContainer* parent, Application* app);
            SDL_Surface* output();
    };
    
}
