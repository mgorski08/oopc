#pragma once

#include <SDL2/SDL.h>

#include "ActionListener.hpp"
#include "Event.hpp"
#include "EventType.hpp"

namespace pac {
    class EventDispatcher {
        private:
            virtual void dispatchEvent(Event* e) = 0;
        public:
            virtual void registerListener(EventType et, ActionListener* l) = 0;
    };
}
