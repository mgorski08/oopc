#pragma once

#include <SDL2/SDL.h>

namespace pac {
    class GraphicsOutput {
        public:
            virtual SDL_Surface* output() = 0;
    };
}
