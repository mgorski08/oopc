#pragma once

#include <SDL2/SDL.h>
#include "Event.hpp"
#include "EventType.hpp"
#include "Widget.hpp"

namespace pac {
    
    class Button: public Widget {
        protected:
            std::string text;
            void setSize(int w, int h);
            void drawNormalBorder();
            void drawDepressedBorder();
            bool isDepressed;
            bool isHighlighted;
            SDL_Rect testRect;
            bool isToggleType;
            
        public:
            Button(WidgetContainer* parent, Application* app, int w, int h, bool t);
            void createSurface();
            //void pressed();
            void actionPerformed(Event* e);
            void resize(int w, int h);
            SDL_Surface* output();
    };
    
}
