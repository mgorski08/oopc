#pragma once

#include <SDL2/SDL.h>

#include "Exception.hpp"
#include "GraphicsInput.hpp"

namespace pac {

    class WindowedGraphicsOutput : public GraphicsInput {
        private:
            SDL_Window* window;
            SDL_Surface* screenSurface;

        public:
            WindowedGraphicsOutput(unsigned int width, unsigned int height);
            ~WindowedGraphicsOutput();
            void input(SDL_Surface* inputSurface);
    };

}
