#pragma once

#include <SDL2/SDL.h>

namespace pac {
    class GraphicsInput {
        public:
            virtual void input(SDL_Surface* inputSurface) = 0;
    };
}
