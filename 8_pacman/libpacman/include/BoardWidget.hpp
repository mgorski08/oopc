#pragma once

#include <SDL2/SDL.h>
#include "Event.hpp"
#include "Board.hpp"
#include "EventType.hpp"
#include "Widget.hpp"

namespace pac {
    
    class BoardWidget: public Widget {
        protected:
            Board board;
            
            int boardWidthBx;
            int boardHeightBx;
            int boxWidthPx;
            int boxHeightPx;
            int boardWidthPx;
            int boardHeightPx;
            
            int lastType;
            
        public:
            BoardWidget(WidgetContainer* parent, Application* app, std::string boardSource);
            void actionPerformed(Event* e);
            SDL_Surface* output();
            void save();
    };
    
}
