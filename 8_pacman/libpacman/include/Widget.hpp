#pragma once

#include <SDL2/SDL.h>

#include "ActionListener.hpp"
#include "Application.hpp"
#include "GraphicsOutput.hpp"

namespace pac {
    class WidgetContainer;

    class Widget : public ActionListener, public GraphicsOutput {
        protected:
            SDL_Surface* background;
            SDL_Surface* surface;
            SDL_Rect rect;
            struct Margin {
                int right;
                int top;
                int left;
                int bottom;
            } margin;
            WidgetContainer* parent;
            Application* app;

        public:
            Widget(WidgetContainer* parent, Application* app) : parent(parent), app(app) {}
            virtual ~Widget();
            virtual SDL_Surface* output() = 0;
            virtual SDL_Rect getRect();
            virtual void setMargin(Margin margin);
            virtual Margin getMargin();
            virtual void moveTo(int x, int y);
            
    };
    
}
