#pragma once

#include "Event.hpp"

namespace pac {
    class ActionListener {
        public:
            virtual void actionPerformed(Event* e) = 0;
    };
}
