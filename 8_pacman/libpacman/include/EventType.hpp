#pragma once

namespace pac {
    enum class EventType {
        MOUSE_CLICKED,
        MOUSE_PRESSED,
        MOUSE_RELEASED,
        MOUSE_MOVED,
        MOUSE_ENTERED,
        MOUSE_EXITED,
        BUTTON_PRESSED,
        QUIT
    };
}
