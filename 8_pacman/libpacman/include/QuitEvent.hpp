#include "Event.hpp"

namespace pac {
    
    class QuitEvent: public Event {
        public:
            
            QuitEvent(EventType type) : Event(type) {}
    };
    
}
