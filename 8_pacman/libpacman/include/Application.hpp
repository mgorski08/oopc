#pragma once

#include <SDL2/SDL.h>
#include <map>
#include <vector>

#include "ActionListener.hpp"
#include "EventType.hpp"
#include "Event.hpp"
#include "EventDispatcher.hpp"
#include "GraphicsInput.hpp"

namespace pac {
    
    class Application : public ActionListener, public EventDispatcher {
        private:
            std::map<EventType, std::vector<ActionListener*> > listeners;
            void dispatchEvent(Event* e);
            
            virtual void procGraphics() = 0;

        public:
            Application();
            Application(int argc, char *argv[]);
            ~Application();
            void registerListener(EventType et, ActionListener* l);
            void actionPerformed(Event* e);
            int exec();
    };

}
