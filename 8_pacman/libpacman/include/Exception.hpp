#ifndef HPP_EXCEPTION
#define HPP_EXCEPTION

#include <string>

namespace pac {

    class Exception {
        private:
            std::string message;

        public:
            Exception();
            Exception(std::string message);
            std::string getMessage();
    };

}

#endif
