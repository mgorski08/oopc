#ifndef HPP_BOARD
#define HPP_BOARD

#include <SDL2/SDL.h>

#include <vector>
#include <string>
#include <iostream>
#include <fstream>

namespace pac {

    class Board {
        private:
            int boardWidthBx;
            int boardHeightBx;
            int boxWidthPx;
            int boxHeightPx;
            int boardWidthPx;
            int boardHeightPx;
            
            int* boardArray;
            
            std::vector<SDL_Surface*> boxes;
            std::vector<std::string> names;
            std::string brdPath;
            
        public:
            Board(std::string filename);
            ~Board();
            
            int getBoardWidthBx();
            int getBoardHeightBx();
            int getBoxWidthPx();
            int getBoxHeightPx();
            int getBoardWidthPx();
            int getBoardHeightPx();
            SDL_Surface* getBoxSurfaceAt(int x, int y);
            int getBoxTypeAt(int x, int y);
            void setBoxTypeAt(int x, int y, int type);
            void changeBoxTypeAt(int x, int y, int amount);
            int getBoxTypesCount();
            void save();
    };

}
#endif
