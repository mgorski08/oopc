#pragma once
#include <SDL2/SDL.h>
#include <map>
#include <vector>

#include "ActionListener.hpp"
#include "EventDispatcher.hpp"
#include "MouseEvent.hpp"
#include "GraphicsOutput.hpp"
#include "Widget.hpp"

namespace pac {

    class WidgetContainer : public Widget, public EventDispatcher {
        protected:
            std::vector<Widget*> children;
            Widget* childInFocus;
            
        public:
            WidgetContainer(WidgetContainer* parent, Application* app) : Widget(parent, app), childInFocus(nullptr) {}
            void actionPerformed(Event* e);
            void dispatchEvent(Event* e);
            void registerListener(EventType et, ActionListener* l);
            void addChild(Widget* widget);
            bool widgetContainsPoint(Widget* w, int x, int y);
            Widget* findChildByPoint(int x, int y);
            void sendMouseEventToChild(Widget* w, MouseEvent* e);
    };
    
}
