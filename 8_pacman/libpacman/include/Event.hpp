#pragma once

#include "EventType.hpp"

namespace pac {
    
    class Event {
        protected:
        
        public:
            EventType type;
            
            Event(EventType type): type(type) {}
    };
    
}
