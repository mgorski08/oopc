#pragma once

#include <SDL2/SDL.h>

#include "Event.hpp"

namespace pac {
    
    class MouseEvent: public Event {
        
        public:
            int x;
            int y;
            enum class ButtonType {
                LEFT,
                MIDDLE,
                RIGHT,
                X1,
                X2,
                NONE
            } buttonType;
            
            MouseEvent(EventType type, int x, int y, ButtonType buttonType = ButtonType::NONE) : Event(type), x(x), y(y), buttonType(buttonType) {}
            
            static ButtonType mouseButtonFromSDL(int button);
    
    };
    
}
