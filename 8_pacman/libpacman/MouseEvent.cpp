#include "include/MouseEvent.hpp"

pac::MouseEvent::ButtonType pac::MouseEvent::mouseButtonFromSDL(int button) {
    switch(button) {
        case SDL_BUTTON_LEFT: return ButtonType::LEFT;
        case SDL_BUTTON_MIDDLE: return ButtonType::MIDDLE;
        case SDL_BUTTON_RIGHT: return ButtonType::RIGHT;
        case SDL_BUTTON_X1: return ButtonType::X1;
        case SDL_BUTTON_X2: return ButtonType::X2;
    }
    return ButtonType::NONE;
}
