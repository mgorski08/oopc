#include "include/BoardWidget.hpp"
#include "include/MouseEvent.hpp"
#include "include/WidgetContainer.hpp"


pac::BoardWidget::BoardWidget(WidgetContainer* parent, Application* app, std::string boardSource) : Widget(parent, app), board(boardSource) {
    boardWidthBx = board.getBoardWidthBx();
    boardHeightBx = board.getBoardHeightBx();
    boardWidthPx = board.getBoardWidthPx();
    boardHeightPx = board.getBoardHeightPx();
    boxWidthPx = board.getBoxWidthPx();
    boxHeightPx = board.getBoxHeightPx();
    
    rect.w = 2*boardWidthPx+1*boardWidthBx-1;
    rect.h = 2*boardHeightPx+1*boardHeightBx-1;
    
    lastType = 0;
    
    
    surface = SDL_CreateRGBSurface(0, rect.w, rect.h, 32, 0, 0, 0, 0);
}

SDL_Surface* pac::BoardWidget::output() {
    SDL_FillRect(surface, nullptr, 0x555555);
    SDL_Rect dstrect;
    SDL_Surface* fBox = board.getBoxSurfaceAt(0, 0);
    dstrect.w = 2*fBox->w;
    dstrect.h = 2*fBox->h;
    for(int y = 0 ; y < boardHeightBx ; y++) {
        for(int x = 0 ; x < boardWidthBx ; x++) {
            dstrect.x = (2*boxWidthPx+1)*x;
            dstrect.y = (2*boxHeightPx+1)*y;
            SDL_BlitScaled(board.getBoxSurfaceAt(x, y), NULL, surface, &dstrect);
        }
    }
    return surface;
}

void pac::BoardWidget::actionPerformed(Event* event) {
    MouseEvent* e = (MouseEvent*) event;
    
    if(e->type == EventType::MOUSE_PRESSED) {
        int x = e->x / (2*board.getBoxWidthPx()+1);
        int y = e->y / (2*board.getBoxHeightPx()+1);
    
        int dir = e->buttonType == MouseEvent::ButtonType::LEFT ? 1 : (e->buttonType == MouseEvent::ButtonType::RIGHT ? -1 : 0);

        if(lastType == board.getBoxTypeAt(x, y)) {
            board.changeBoxTypeAt(x, y, dir);
            lastType = board.getBoxTypeAt(x, y);
        } else {
            board.setBoxTypeAt(x, y, lastType);
        }
    }
}

void pac::BoardWidget::save() {
    board.save();
}
