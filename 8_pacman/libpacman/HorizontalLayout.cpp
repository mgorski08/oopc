#include "include/HorizontalLayout.hpp"
#include "include/EventType.hpp"

pac::HorizontalLayout::HorizontalLayout(WidgetContainer* parent, Application* app) : WidgetContainer(parent, app) {
    app->registerListener(EventType::MOUSE_MOVED, this);
    app->registerListener(EventType::MOUSE_PRESSED, this);
    app->registerListener(EventType::MOUSE_RELEASED, this);
    background = SDL_CreateRGBSurface(0, 768, 576, 32, 0, 0, 0, 0);
    surface = SDL_CreateRGBSurface(0, 768, 576, 32, 0, 0, 0, 0);
}

SDL_Surface* pac::HorizontalLayout::output() {
    SDL_FillRect(surface, NULL, 0xC3C3C3);
    for(Widget* child: children) {
        SDL_Rect tmpRect = child->getRect();
        SDL_BlitSurface(child->output(), NULL, surface, &tmpRect);
    }
    return surface;
}
