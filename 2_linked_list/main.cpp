#include <iostream>

#include "List.hpp"

int main()
{
 List origList;
 origList.insert(1);
 origList.insert(2);
 origList.insert(3);
 origList.goToHead();
 
 List l;
 l.insert(5);
 l.insert(5);
 l.insert(5);
 l.insert(5);
 l.insert(5);
 l.insert(5);
 l.insert(5);
 l.insert(5);
 l.insert(5);
 l = origList;
 
 while(l.moreData())
 {
   int val;
   val=l.getCurrentData();
   std::cout << val << " ";
   l.advance();
 }
 std::cout << std::endl;
}
