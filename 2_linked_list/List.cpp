#include <iostream>

#include "List.hpp"

List::List () {
  head = NULL;
  current = NULL;
}

List::~List () {
  while (head)
    {
      node *t = head->next;
      delete head;
      head = t;
    };
}

void List::insert (int a) {
  node *t = new node;
  t->next = head;
  head = t;
  head->val = a;
}

void List::goToHead () {
  current = head;
}

int List::getCurrentData () {
  return current->val;
}

void List::advance () {
  current = current->next;
}

bool List::moreData () {
  if (current)
    return true;
  else
    return false;
}

List::List (const List & l) {
  current=NULL;
  node *src, **dst;
  head = NULL;
  src = l.head;
  dst = &head;
  while (src)
    {
      *dst = new node;
      (*dst)->val = src->val;
      (*dst)->next = NULL;
      if(src==l.current)
        current=*dst;
      src = src->next;
      dst = &((*dst)->next);
    }
}

List & List::operator= (const List & l) {
    current = NULL;
    if (&l == this) return *this;
    node *src, **dst;
    src = l.head;
    dst = &head;
    while(src && *dst) {
        (*dst)->val = src->val;
        if(l.current == src) {
            current = *dst;
        }
        src = src->next;
        dst = &((*dst)->next);
    }
    
    if(src == NULL) {
        while (*dst) {
            node *t = (*dst)->next;
            delete *dst;
            *dst = t;
        }
    }
    
    if(*dst == NULL) {
        while(src) {
            *dst = new node;
            (*dst)->val = src->val;
            (*dst)->next = NULL;
            if(src==l.current) current=*dst;
            src = src->next;
            dst = &((*dst)->next);
        }
    }
    
    return *this;
}
