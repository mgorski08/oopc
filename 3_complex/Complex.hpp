class Complex {
private:
    double real;
    double imag;
public:
  Complex();
  Complex(double number);
  Complex(double real, double imag);
  Complex& operator=(const Complex& s);
  Complex operator-() const;
  Complex& operator= (double number);
  Complex& operator-= (Complex co);
  Complex& operator+= (Complex co);
  Complex& operator*= (Complex co);
  Complex& operator/= (Complex co);
  double re();
  double im();
  double magnitude();
  double phase();
  
  friend Complex operator+(Complex co1, Complex co2);
  friend Complex operator-(Complex co1, Complex co2);
  friend Complex operator*(Complex co1, Complex co2);
  friend Complex operator/(Complex co1, Complex co2);
  friend bool operator==(Complex co1, Complex co2);
  
  friend std::ostream& operator<<(std::ostream & s, const Complex & c);
  
};
