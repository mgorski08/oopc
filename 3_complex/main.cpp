#include <iostream>

#include "Complex.hpp"

int main() {
    Complex a(1,2),b(1,3);
    Complex c = a+b;
    std::cout << a << std::endl;
    std::cout << b << std::endl;
    std::cout << c << std::endl;
}
