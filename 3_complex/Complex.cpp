#include <iostream>
#include <cmath>

#include "Complex.hpp"

Complex::Complex():real(0), imag(0)
    { }

Complex::Complex(double number) {
    real = number;
    imag = 0;
}

Complex::Complex(double real, double imag) {
    this->real = real;
    this->imag = imag;
}

Complex& Complex::operator=(const Complex& s) {
    real = s.real;
    imag = s.imag;
    return *this; 
}

Complex Complex::operator-() const {
    return Complex(-real,-imag);
}

Complex & Complex::operator=(double number) {
    real = number;
    imag = 0;
    return *this;
}

Complex& Complex::operator-=(Complex co) {
    real -= co.real;
    imag -= co.imag;
    return *this;
}

Complex& Complex::operator+=(Complex co) {
    real += co.real;
    imag += co.imag;
    return *this;
}

Complex& Complex::operator*=(Complex co) {
    double r = real * co.real - imag * co.imag;
    double i = real * co.imag + imag * co.real;
    real = r;
    imag = i;
    return *this;
}

Complex& Complex::operator/=(Complex co) {
    double r = (real * co.real + imag * co.imag)/(co.real * co.real + co.imag * co.imag);
    double i = (imag * co.real - real * co.imag)/(co.real * co.real + co.imag * co.imag);
    real = r;
    imag = i;
    return *this;
}

Complex operator+(Complex co1, Complex co2) {
    return co1 += co2;
}

Complex operator-(Complex co1, Complex co2) {
    return co1 -= co2;
}

Complex operator*(Complex co1, Complex co2) {
    return co1 *= co2;
}

Complex operator/(Complex co1, Complex co2) {
    return co1 /= co2;
}

bool operator==(Complex co1, Complex co2) {
    if(co1.real == co2.real && co1.imag == co2.imag) {
      return true;
    }
    return false;
}

double Complex::re() {
    return real;
}

double Complex::im() {
    return imag;
}

double Complex::magnitude() {
    return sqrt(real*real+imag*imag);
}

double Complex::phase() {
    return atan2(imag, real);
}


std::ostream& operator<<(std::ostream & s, const Complex & c) {
    s << "(" << c.real << "," << c.imag << ")";
    return s;
}
