#include <map>
#include <string>

class Poly {
private:
    std::map<int, double, std::greater<int> > polyMap;
    
    static void printElement(std::ostream& stream, double multiplier, double exponent);
    
    void clearZeros();
public:
    Poly();
    Poly(int i);
    Poly(const Poly& p);
    ~Poly();
    double& operator[](const unsigned int index);
    
    Poly operator-() const;
    double operator()(double x) const;
    
    friend Poly operator+(const Poly& p1, const Poly& p2);
    friend Poly operator-(const Poly& p1, const Poly& p2);
    friend Poly operator*(const Poly& p1, const Poly& p2);
    
    Poly& operator=(const Poly& p);
    
    friend std::ostream& operator<<(std::ostream& stream, Poly p);
    static std::string elementToString(double multiplier, int exponent);
    static void subscriptNumeral(std::string& str);
};
