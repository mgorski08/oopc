#include <iostream>
#include <cmath>
#include <string>
#include <sstream>

#include "Poly.hpp"


Poly::Poly() {
    
}

Poly::Poly(const Poly& p) {
    polyMap = p.polyMap;
}

Poly::Poly(int i) {
    polyMap[0] = i;
}

Poly::~Poly() {
    
}


double& Poly::operator[](const unsigned int index) {
    return polyMap[index];
}

Poly& Poly::operator=(const Poly& p) {
    polyMap = p.polyMap;
    return *this;
}

void Poly::printElement(std::ostream& stream, double multiplier, double exponent) {
    if(multiplier == 0) {stream << "0"; return;}
    if(exponent == 0) {stream << multiplier; return;}
    if(exponent == 1) {stream << multiplier << "x"; return;}
    stream << multiplier << "x^" << exponent;
}

std::string Poly::elementToString(double multiplier, int exponent) {
    if(multiplier == 0) return "0";
    
    std::ostringstream out;
    if(multiplier == -1) {
        out << "-";
    } else if(multiplier != 1) { //if is 1 leave empty
        out << multiplier;
    }
    
    if(exponent == 0) return out.str();
    out << "x";
    if(exponent == 1) return out.str();
    
    std::ostringstream exponentStr;
    exponentStr << exponent;
    
    
    std::string str = std::string(exponentStr.str());
    subscriptNumeral(str);
    out<<str;
    
    return out.str();
}

void Poly::clearZeros() {
    auto tmp = polyMap;
    for(auto a : polyMap) {
        if(a.second == 0) tmp.erase(a.first);
    }
    polyMap = tmp;
}

std::ostream& operator<<(std::ostream& stream, Poly p) {
    bool first = true;
    for(auto a : p.polyMap) {
        std::string str = Poly::elementToString(a.second, a.first);
        if(first) {
            stream << str;
        } else {
            if(str[0] == '-') {
                str.erase(0, 1);
                stream << " - " << str;
            } else {
                stream << " + " << str;
            }
        }
        
        first = false;
    }
    return stream;
}

Poly Poly::operator-() const {
    Poly newPoly = *this;
    for(auto a : newPoly.polyMap) {
        newPoly[a.first] = -a.second;
    }
    return newPoly;
}

double Poly::operator()(double x) const {
    double val = 0;
    for(auto a : polyMap) {
        val += a.second*pow(x, a.first);
    }
    return val;
}



Poly operator+(const Poly& p1, const Poly& p2) {
    Poly newPoly = p2;
    for(auto a : p1.polyMap) {
        newPoly[a.first] += a.second;
    }
    newPoly.clearZeros();
    return newPoly;
}

Poly operator-(const Poly& p1, const Poly& p2) {
    Poly newPoly = p1;
    for(auto a : p2.polyMap) {
        newPoly[a.first] -= a.second;
    }
    newPoly.clearZeros();
    return newPoly;
}

Poly operator*(const Poly& p1, const Poly& p2) {
    Poly newPoly;
    for(auto a : p1.polyMap) {
        for(auto b : p2.polyMap) {
            newPoly[a.first + b.first] += (a.second * b.second);
        }
    }
    newPoly.clearZeros();
    return newPoly;
}

void Poly::subscriptNumeral(std::string& str) {
    for (int i = 0 ; str[i] != 0 ; i++) {
        switch(str[i]) {
            case '0' : str.erase(i, 1); str.insert(i, "⁰"); break;
            case '1' : str.erase(i, 1); str.insert(i, "¹"); break;
            case '2' : str.erase(i, 1); str.insert(i, "²"); break;
            case '3' : str.erase(i, 1); str.insert(i, "³"); break;
            case '4' : str.erase(i, 1); str.insert(i, "⁴"); break;
            case '5' : str.erase(i, 1); str.insert(i, "⁵"); break;
            case '6' : str.erase(i, 1); str.insert(i, "⁶"); break;
            case '7' : str.erase(i, 1); str.insert(i, "⁷"); break;
            case '8' : str.erase(i, 1); str.insert(i, "⁸"); break;
            case '9' : str.erase(i, 1); str.insert(i, "⁹"); break;
        }
    }
}
