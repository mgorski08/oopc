#pragma once
#include <iostream>

class Employee {
	public:
	std::string name;
	std::string position;
	unsigned int age;
	
	Employee():name(""), position(""), age(0){};
	Employee(std::string name, std::string position, unsigned int age): name(name), position(position), age(age){};
	friend std::ostream& operator << (std::ostream& out, const Employee& emp) {
		out << emp.name << std::endl;
		out << emp.position << std::endl;
		out << emp.age << std::endl;
		return out;
	}
};
