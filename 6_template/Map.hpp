#pragma once
#include <iostream>

template<class K, class E> class Map {
	private:
		struct node {
			K key;
			E val;
			node* next;
			
			node(K key, E val, node* next): key(key), val(val), next(next){};
			node(const node& n): key(n.key), val(n.val), next(n.next){};
		};
		node* head;
		node* current;
	public:
		Map() {
			head = NULL;
			current = NULL;
		}
		
		Map(const Map& map) {
			current=NULL;
			node *src, **dst;
			head = NULL;
			src = map.head;
			dst = &head;
			while (src) {
				*dst = new node(*src);
				(*dst)->next = NULL;
				if(src==map.current) current=*dst;
				src = src->next;
				dst = &((*dst)->next);
			}
		}
		
		Map(node* n) {
			head = n;
			current = n;
		}
		
		~Map() {
			while (head != NULL) {
				node *t = head->next;
				delete head;
				head = t;
			}
		}
		
		Map& operator=(Map& map) {
			Map tmp(head);
			
			head=NULL;
			
			map.goToHead();
			while(map.hasMoreData()) {
				add(map.current->key, map.current->val);
				map.advance();
			}
			return *this;
		}
		
		void add(K key, E val) {
			  node *t = new node(key, val, head);
			  head = t;
		}
		
		void advance() {
			current = current->next;
		}

		bool hasMoreData() {
			if (current) return true; else return false;
		}
		
		void goToHead() {
			current = head;
		}
		
		E* find(K key) {
			goToHead();
			while(hasMoreData() && current->key != key) {
				advance();
			}
			if(current->key == key) {
				return &(current->val);
			} else {
				return NULL;
			}
		}
		
		friend std::ostream& operator << (std::ostream& out, Map& map) {
			map.goToHead();
			while(map.hasMoreData()) {
				out << map.current->key << ":" << map.current->val << std::endl;
				map.advance();
			}
			return out;
		}
};



