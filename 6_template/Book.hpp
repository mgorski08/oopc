#pragma once
#include <iostream>

class Book {
	public:
	std::string author;
	std::string category;
	unsigned int pages;
	enum STATUS {BOROWED, ON_SHELF} status;
	
	Book():author(""), category(""), pages(0), status(){};
	Book(std::string author, std::string category, unsigned int pages, STATUS status): author(author), position(position), age(age){};
	friend std::ostream& operator << (std::ostream& out, const Employee& emp) {
		out << emp.name << std::endl;
		out << emp.position << std::endl;
		out << emp.age << std::endl;
		return out;
	}
};
